%% SEQuence dot plot >>> for the capability of my laptop:
function [A, B, zerosPlane] = plotFigures(A,B)

% plotFigures(A,B); function responsible for generating the output
%                   NON-FILTERED dotplot; 

%A = cellstr('ATTGCTTATGGATTGCG');
%B = cellstr('AGGGCATTATGGATTGCGAAA');

Achar = char(A);
Bchar = char(B);

% planeSize as a matrix containing the zerosPlane size nxm; 
planeSize = [length(char(A)), length(char(B))];
% crucial zerosPlane matrix assign;
zerosPlane = zeros(planeSize);

% double for/nested loop instruction in order to overwrite 0s with 1s
% where the sequence's nucleotide identity occurs;
for i = 1:1:length(char(A))
    for j = 1:1:length(char(B))
        
        if strcmp(Achar(i),Bchar(j))==1
           zerosPlane(i,j) =1;
        end
        
    end
end

% plot zeroPlane
figure('Name', 'FASTA content comparison'); 
spy(zerosPlane);
title('seq. content comparison');
xlabel(sprintf('indices of nucleotides of the 1. user input sequence'));
ylabel(sprintf('indices of nucleotides of the 2. user input sequence'));

end