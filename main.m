%% TESTINIG *.M.file !!!
clc;
clear variables;
%% SEQuence read file;

[firstSeq, secondSeq] = FASTAreadManual();

%% SEQuence dot plot; - compare FASTAcontent similarity t.b.c.

% planeSize = [length(char(firstSeq)), length(char(secondSeq))]
% zerosPlane = zeros(planeSize);

%% SEQuence dot plot >>> for the capability of my laptop:

A = cellstr('ATTGCTTATGGA');
B = cellstr('ATTGCATATG');

Achar = char(A);
Bchar = char(B);

planeSize = [length(char(A)), length(char(B))];
zerosPlane = zeros(planeSize);

for i = 1:1:length(char(A))
    for j = 1:1:length(char(B))
        
        if strcmp(Achar(i),Bchar(j))==1
           zerosPlane(i,j) =1;
        end
        
    end
end



figure('Name', 'FASTA content comparison');
xlabel('indices of 1. user input sequence of nucleobases') 
ylabel('indices of 2. user input sequence of nucleobases') 
spy(zerosPlane);