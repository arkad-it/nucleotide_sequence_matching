%% filtering;
function [] = filtering(A, B, zerosPlane, windowSize, errorMargin)

% function filtering(...); is responsible for deleting redundant artifacts
%                          from the zerosPlane matrix allowing analysis to
%                          become more transparent in study;

%windowSize = 5;
%errorMargin = 2;

sizeA = length(char(A)) + 1 - windowSize;
sizeB = length(char(B)) + 1 - windowSize;

% understood as the filtering window's bottom edge;
windowRowDown = windowSize-1;
% -||- upper edge;
windowRowUp = 0;
% -||- left edge;
windowColLeft = 0;
% -||- right edge;
windowColRight = windowSize-1;

for i = 1:1:sizeA % reallocating the window vertically;
    
    windowRowUp = windowRowUp + 1;
    windowRowDown = windowRowDown + 1;
    
    for j = 1:1:sizeB % reallocating the window vertically;
        
        windowColLeft = windowColLeft + 1;
        windowColRight = windowColRight + 1;
        
        window = zerosPlane(windowRowUp:windowRowDown, windowColLeft:windowColRight);
        diagWindow = transpose(diag(window));
        zerosNum = nnz(~diagWindow); % getting the number of zeros in the diagnoal;
        
        if (diagWindow(1)==0) && (diagWindow(end)==0) && (zerosNum > errorMargin)
            
            % if conditions are true for the case >>> clear arifacs from
            % the base matrix (zerosPlane) within the filter window area;
            zerosPlane(windowRowUp:windowRowDown, windowColLeft:windowColRight) = ...
                zerosPlane(windowRowUp:windowRowDown, windowColLeft:windowColRight) .* ~eye(windowSize);
            
        end
        
    end
    
    windowColLeft = 0;
    windowColRight = windowSize-1;

end

% genereting filtered zerosPlane matrix over the NON-filtered one;

figure('Name', 'seq. content comparison - filtered'); 
spy(zerosPlane);
title('seq. content comparison - filtered');
xlabel(sprintf('indices of nucleotides of the 1. user input sequence'));
ylabel(sprintf('indices of nucleotides of the 2. user input sequence'));

%fileName = 'file1';
%saveas(gcf,fileName,'jpg');

end